﻿namespace MyBank.BankDb
{
    public static class Maths
    {
        public static int AddIntegers(int first, int second)
        {
            int sum = first;
            for (int i = 0; i < second; i++)
            {
                sum += 1;
            }

            return sum;
        }
    }
}