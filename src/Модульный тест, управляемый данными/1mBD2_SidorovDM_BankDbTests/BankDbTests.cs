using MyBank.BankDb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using System;
using System.Collections.Generic;

namespace _1mBD2_SidorovDM_BankDbTests
{
    [TestClass]
    public class BankDbTests
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        [DataTestMethod]
        [DataRow(1, 1, 2)]
        [DataRow(2, 2, 4)]
        [DataRow(3, 3, 6)]
        //[DataRow(0, 0, 1)] // The test run with this row fails
        public void AddIntegers_FromDataRowTest(int x, int y, int expected)
        {
            int actual = Maths.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }

        public static IEnumerable<object[]> AdditionData
        {
            get
            {
                return new[]
                {
                    new object[] { 1, 1, 2 },
                    new object[] { 2, 2, 4 },
                    new object[] { 3, 3, 6 },
                    //new object[] { 0, 0, 1 }, // The test run with this row fails
                };
            }
        }

        public static string GetCustomDynamicDataDisplayName(MethodInfo methodInfo, object[] data)
        {
            return string.Format("DynamicDataTestMethod {0} with {1} parameters",
                methodInfo.Name, data.Length);
        }


        [TestMethod]
        [DynamicData(nameof(AdditionData), DynamicDataDisplayName = nameof(GetCustomDynamicDataDisplayName))]
        public void AddIntegers_FromDynamicDataTest(int x, int y, int expected)
        {
            int actual = Maths.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }

        private static string path = @"";

        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
  @".\sidorov_numbers.csv;", "sidorov_numbers#csv", DataAccessMethod.Sequential)]
        public void AddIntegers_FromDataSourceTest()
        {
            // Access the data
            var rows = TestContext.DataRow[0].ToString().Split(';');
            int x = Convert.ToInt32(rows[0]);
            int y = Convert.ToInt32(rows[1]);
            int expected = Convert.ToInt32(rows[2]);
            int actual = Maths.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }

    }
}